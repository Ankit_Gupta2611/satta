import React from "react";
import { Button } from "react-native-paper";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  View,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput,
  Dimensions,
} from "react-native";
import Colors from "../constants/Colors";
const screenHeight = Dimensions.get("window").height;

const ForgetPasswordScreen = () => {
  return (
    <KeyboardAwareScrollView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={require("../assets/banner/login1.png")}
        />
        <View style={styles.content}>
          <Text style={{ fontSize: 20, textAlign: "center", marginTop: 2 }}>
            Forgot Your Password?
          </Text>
          <Text style={{ textAlign: "center", marginTop: 10, paddingHorizontal:30}}>
            To recover your password, you need to enter your registered email
            address. We will sent the recovery code to your email
          </Text>
          <View style={styles.content}>
            <TextInput placeholder="Email" style={styles.username} />
            <Button mode="contained" style={styles.sendButton}>
            <Text style={styles.sendText}>SEND</Text>
          </Button>
          </View>
          
        </View>
        <View style={styles.bottomBox}>
          <Text>Don't have an Account?</Text><Text style={{fontWeight:'bold'}}>SIGN UP</Text>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: screenHeight,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  username: {
    padding: 10,
    backgroundColor: Colors.input,
    width: "80%",
    height: 43,
    borderRadius: 10,
    marginVertical: 15,
    textAlign:"center"
  },
  sendButton: {
    borderRadius: 10,
    backgroundColor: Colors.primary,
    width: "80%",
    // height: 50,
    borderWidth: 0.3,
    borderColor: "white",
    justifyContent: "center",
    alignItems: "center",
    // marginBottom: 40,

  },
  sendText: {
    color: Colors.white,
  },
  bottomBox: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    position: "absolute",
    bottom: 0,
    },
    
});
export default ForgetPasswordScreen;
