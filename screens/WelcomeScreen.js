import React from "react";
import { Button } from 'react-native-paper';
import {
  View,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  } from "react-native";
const image = require("../assets/banner/welcome.png");
import Colors from "../constants/Colors";

const WelcomeScreen = () => {
  return (
    <View style={styles.container}>
    <View style={styles.contentBox}>
        <ImageBackground source={image} style={styles.imageBack}> 
        <View style={styles.textContent}>
            <Text style={styles.welcomeText}>Welcome!</Text>
            <Text style={styles.description}>Play and Win</Text>
        </View>
        <Button mode="contained" style={styles.signupButton}>
            <Text style={styles.signupText}>SIGN UP</Text>
        </Button>
        <Button mode="contained" style={styles.loginButton}>
            <Text style={styles.loginText}>LOG IN</Text>
        </Button>
        </ImageBackground>
        
    </View>
    </View>
    
  );


};

const styles= StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: "column",
    },
    imageBack: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        width:"100%"
      },
    contentBox: {
        justifyContent: "space-around",
        alignItems: "center",
        flex: 1,
      },
      textContent: { flex: 1, justifyContent: "center" },
      buttonContent: {
        flex: 1,
        alignItems: "center",
      },
      welcomeText: {
        marginTop:300,
        fontSize: 50,
        color: "white",
        fontWeight: "bold",
        textAlign:"center"
        },
      description: {
        fontSize: 35,
        color: "white",
        textAlign:"center"
      },
      signupButton: {
        borderRadius: 10,
        backgroundColor: Colors.primary,
        width: 300,
        height:50,
        borderWidth: 0.3,
        borderColor: "white",
        justifyContent: "center",
        alignItems:"center",
        marginVertical:15,
      },
      signupText: {
        color: Colors.blue,
      },
      loginButton: {
        borderRadius: 10,
        backgroundColor: "white",
        width: 300,
        borderWidth: 0.3,
        alignItems:"center",
        marginBottom:15,
        height:50,
      },
      loginText: { color: Colors.lightblue, textAlign: "center" }

})

export default WelcomeScreen;
