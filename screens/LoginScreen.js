import React from "react";
import { Button } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  View,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput ,
  Dimensions
  } from "react-native";
import Colors from "../constants/Colors";
const image = require("../assets/banner/login.png");
const screenHeight = Dimensions.get('window').height

const LoginScreen = () => {
    return(
        <KeyboardAwareScrollView style={{flex:1}}> 
        <View style={styles.container}>
            <Image source={require("../assets/banner/login1.png")}/>
            <View style={styles.content}>
                <TextInput placeholder="Username" style={styles.username} />
                <TextInput placeholder="Password" style={styles.username}/>
                <Button mode="contained" style={styles.signinButton}>
                    <Text style={styles.signinText}>SIGN IN</Text>
                </Button>
                <Text style={{marginTop:20}}>Forget Password?</Text>
                
            </View>
            <View>
                   
            </View>
            <View style={styles.bottomBox}>
          <Text>Don't have an Account?</Text>
            <Text style={styles.signupText}> SIGN UP</Text>
          </View>
            

        </View>
        </KeyboardAwareScrollView>
            

        
        
    );
};

const styles=StyleSheet.create({
    container: {
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: screenHeight,
  },

    username: {
    padding: 10,
    backgroundColor: Colors.input,
    width: "80%",
    height: 43,
    borderRadius: 10,
    marginVertical:5,

  },
  content: {  
    flex: 1,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  signinButton:{
      borderRadius: 10,
      width: "80%",
      marginVertical:5,
      backgroundColor: Colors.primary,
  },
  bottomBox: {
    flex: 1,
    flexDirection: "row",
    justifyContent: 'flex-end',
    position:'absolute',
    bottom:0
  } 
})
export default LoginScreen;