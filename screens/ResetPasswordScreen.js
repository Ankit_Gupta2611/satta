import React from "react";
import { Button } from "react-native-paper";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  View,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput,
  Dimensions,
} from "react-native";
import Colors from "../constants/Colors";
const screenHeight = Dimensions.get("window").height;

const ResetPasswordScreen = () => {
  return (
    <KeyboardAwareScrollView style={{ flex: 1 }}>
        <View style={styles.container}>
        <Image
          style={styles.image}
          source={require("../assets/banner/login1.png")}
        />
        <View style={styles.content}>
          <Text style={{ fontSize: 20, textAlign: "center", marginTop: 10 }}>
            Successfully Verified
          </Text>
          <View style={styles.content}>
            <TextInput placeholder="Enter New Password" style={styles.username} />
            <TextInput placeholder="Cnofirm New Password" style={styles.username} />
            <Button mode="contained" style={styles.sendButton}>
            <Text style={styles.sendText}>Reset Password</Text>
          </Button>
          </View>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: screenHeight,
  },
  content: {
    flex: 1,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  username: {
    padding: 10,
    backgroundColor: Colors.input,
    width: "80%",
    height: 43,
    borderRadius: 10,
    marginVertical: 10,
    textAlign:"center"
  },
  sendButton: {
    borderRadius: 10,
    backgroundColor: Colors.primary,
    width: "80%",
    // height: 50,
    borderWidth: 0.3,
    borderColor: "white",
    justifyContent: "center",
    alignItems: "center",
    // marginBottom: 40,

  },
  sendText: {
    color: Colors.white,
  },
  bottomBox: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    position: "absolute",
    bottom: 0,
    
    },
    image: {
        // marginTop:60,
        // width:"60%",
    }
});
export default ResetPasswordScreen;
