const Colors = {
  blue: "#1E3799",
  primary: "#feca57",
  secondary: "#ff9f43",
  lightblue: "#434343",
  input: "#E6EAEE",
  textLight: "#767D89",
  lightGreen: "#2ecc71",
  white: "#fff",
};

export default Colors;

